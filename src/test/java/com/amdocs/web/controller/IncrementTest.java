package com.amdocs.web.controller;

import com.amdocs.Increment;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
@SuppressWarnings("PMD")
public class IncrementTest {

    @Test
    public void indexIsZeroTest() throws Exception {
        assertEquals("Result", 1, new Increment().decreasecounter(0));
    }

    @Test
    public void indexIsOneTest() throws Exception {
        assertEquals("Result", 1, new Increment().decreasecounter(1));
    }

    @Test
    public void indexIsTwoTest() throws Exception {
        assertEquals("Result", 1, new Increment().decreasecounter(2));
    }

}
