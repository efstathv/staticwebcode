package com.amdocs.web.controller;

import com.amdocs.Calculator;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
@SuppressWarnings("PMD")
public class CalculatorTest {


    @Test
    public void addTest() throws Exception {
        assertEquals("Result", 9, new Calculator().add());
    }

    @Test
    public void subTest() throws Exception {
        assertEquals("Result", 3, new Calculator().sub());
    }
}
